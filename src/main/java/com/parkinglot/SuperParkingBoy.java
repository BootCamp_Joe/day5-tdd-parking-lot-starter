package com.parkinglot;

import java.util.Comparator;

public class SuperParkingBoy extends ParkingBoy{
    public SuperParkingBoy() {}

    @Override
    public ParkingLot getAvailabeParkingLot() {
        return parkingLots.stream()
                .max(Comparator.comparingDouble(parkingLot -> (double) parkingLot.getNumberOfEmptyPositions() /parkingLot.getCapacity()))
                .orElse(null);
    }
}
