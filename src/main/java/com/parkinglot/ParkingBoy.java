package com.parkinglot;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

public abstract class ParkingBoy {
    protected List<ParkingLot> parkingLots = new ArrayList<>();

    public void manageParkingLot(ParkingLot parkingLot) {
        parkingLot.setId(parkingLots.isEmpty()? 1 : parkingLots.get(parkingLots.size() - 1).getId() + 1);
        parkingLots.add(parkingLot);
    }

    public ParkingTicket park(Car car) {
        ParkingLot availableParkingLot = getAvailabeParkingLot();
        if(Objects.isNull(availableParkingLot)) {
            throw new UnrecognizedTicketException("No available position.");
        }
        return availableParkingLot.park(car);
    }

    public Car fetch(ParkingTicket parkingTicket) {
        Car parkedCar = parkingLots.stream()
                .map(parkingLot -> parkingLot.fetch(parkingTicket))
                .flatMap(Optional::stream)
                .findFirst()
                .orElseThrow(() -> new UnrecognizedTicketException("Unrecognized parking ticket."));

        return parkedCar;
    }

    public abstract ParkingLot getAvailabeParkingLot();
}
