package com.parkinglot;

public class StandardParkingBoy extends ParkingBoy {
    public StandardParkingBoy() {}

    @Override
    public ParkingLot getAvailabeParkingLot() {
        return parkingLots.stream().filter(parkingLot -> !parkingLot.isParkingLotFull()).findFirst().orElse(null);
    }
}
