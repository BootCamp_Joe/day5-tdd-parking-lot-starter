package com.parkinglot;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SmartParkingBoyTest {
    @Test
    void should_park_at_first_parking_lot_when_park_given_two_available_parking_lots() {
        //given
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy();
        smartParkingBoy.manageParkingLot(new ParkingLot());
        smartParkingBoy.manageParkingLot(new ParkingLot());
        Car car = new Car();

        //when
        ParkingTicket parkingTicket = smartParkingBoy.park(car);

        //then
        assertEquals(1, parkingTicket.getParkingLotId());
    }
    @Test
    void should_park_at_second_parking_lot_when_park_given_full_and_available_parking_lots() {
        //given
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy();
        smartParkingBoy.manageParkingLot(new ParkingLot(1));
        smartParkingBoy.manageParkingLot(new ParkingLot());
        Car car1 = new Car();
        Car car2 = new Car();
        smartParkingBoy.park(car1);

        //when
        ParkingTicket parkingTicket = smartParkingBoy.park(car2);

        //then
        assertEquals(2, parkingTicket.getParkingLotId());
    }

    @Test
    void should_return_corresponding_car_when_fetch_given_two_parking_lots_and_two_tickets_and_two_cars() {
        //given
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy();
        smartParkingBoy.manageParkingLot(new ParkingLot(5));
        smartParkingBoy.manageParkingLot(new ParkingLot(5));
        Car car1 = new Car();
        ParkingTicket parkingTicket1 = smartParkingBoy.park(car1);

        Car car2 = new Car();
        ParkingTicket parkingTicket2 = smartParkingBoy.park(car2);

        assertEquals(1, parkingTicket1.getParkingLotId());
        assertEquals(2, parkingTicket2.getParkingLotId());

        //when
        Car actualCar1 = smartParkingBoy.fetch(parkingTicket1);
        Car actualCar2 = smartParkingBoy.fetch(parkingTicket2);

        //then
        assertEquals(car1, actualCar1);
        assertEquals(car2, actualCar2);
    }

}
