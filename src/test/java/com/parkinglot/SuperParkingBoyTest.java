package com.parkinglot;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SuperParkingBoyTest {
    @Test
    void should_park_at_first_parking_lot_when_park_given_two_available_parking_lots() {
        //given
        SuperParkingBoy superParkingBoy = new SuperParkingBoy();
        superParkingBoy.manageParkingLot(new ParkingLot());
        superParkingBoy.manageParkingLot(new ParkingLot());
        Car car = new Car();

        //when
        ParkingTicket parkingTicket = superParkingBoy.park(car);

        //then
        assertEquals(1, parkingTicket.getParkingLotId());
    }

    @Test
    void should_park_at_second_parking_lot_when_park_given_full_and_available_parking_lots() {
        //given
        SuperParkingBoy superParkingBoy = new SuperParkingBoy();
        superParkingBoy.manageParkingLot(new ParkingLot(1));
        superParkingBoy.manageParkingLot(new ParkingLot());
        Car car1 = new Car();
        Car car2 = new Car();
        superParkingBoy.park(car1);

        //when
        ParkingTicket parkingTicket = superParkingBoy.park(car2);

        //then
        assertEquals(2, parkingTicket.getParkingLotId());
    }

    @Test
    void should_return_corresponding_car_when_fetch_given_two_parking_lots_and_five_tickets_and_five_cars() {
        //given
        SuperParkingBoy superParkingBoy = new SuperParkingBoy();
        superParkingBoy.manageParkingLot(new ParkingLot(4));
        superParkingBoy.manageParkingLot(new ParkingLot(8));
        Car car1 = new Car();
        ParkingTicket parkingTicket1 = superParkingBoy.park(car1);

        Car car2 = new Car();
        ParkingTicket parkingTicket2 = superParkingBoy.park(car2);

        Car car3 = new Car();
        ParkingTicket parkingTicket3 = superParkingBoy.park(car3);

        Car car4 = new Car();
        ParkingTicket parkingTicket4 = superParkingBoy.park(car4);

        Car car5 = new Car();
        ParkingTicket parkingTicket5 = superParkingBoy.park(car5);

        assertEquals(1, parkingTicket1.getParkingLotId());
        assertEquals(2, parkingTicket2.getParkingLotId());
        assertEquals(2, parkingTicket3.getParkingLotId());
        assertEquals(1, parkingTicket4.getParkingLotId());
        assertEquals(2, parkingTicket5.getParkingLotId());

        //when
        Car actualCar1 = superParkingBoy.fetch(parkingTicket1);
        Car actualCar2 = superParkingBoy.fetch(parkingTicket2);
        Car actualCar3 = superParkingBoy.fetch(parkingTicket3);
        Car actualCar4 = superParkingBoy.fetch(parkingTicket4);
        Car actualCar5 = superParkingBoy.fetch(parkingTicket5);

        //then
        assertEquals(car1, actualCar1);
        assertEquals(car2, actualCar2);
        assertEquals(car3, actualCar3);
        assertEquals(car4, actualCar4);
        assertEquals(car5, actualCar5);
    }
}
