package com.parkinglot;

public class Car {
    private Integer ticketId;
    private Integer parkingLotId;

    public Integer getTicketId() {
        return ticketId;
    }

    public Integer getParkingLotId() {
        return parkingLotId;
    }

    public void setTicketId(Integer ticketId) {
        this.ticketId = ticketId;
    }

    public void setParkingLotId(Integer parkingLotId) {
        this.parkingLotId = parkingLotId;
    }
}
