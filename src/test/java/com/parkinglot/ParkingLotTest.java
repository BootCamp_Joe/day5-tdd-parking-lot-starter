package com.parkinglot;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ParkingLotTest {

    @Test
    void should_return_ticket_when_park_given_parking_lot_and_car_and_parking_boy(){
        //given
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy();
        standardParkingBoy.manageParkingLot(new ParkingLot());
        Car car = new Car();

        //when
        ParkingTicket parkingTicket = standardParkingBoy.park(car);

        //then
        assertNotNull(parkingTicket);

    }

    @Test
    void should_return_car_when_fetch_given_parking_lot_and_ticket(){
        //given
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy();
        standardParkingBoy.manageParkingLot(new ParkingLot());
        Car car = new Car();
        ParkingTicket parkingTicket = standardParkingBoy.park(car);

        //when
        Car actual = standardParkingBoy.fetch(parkingTicket);

        //then
        assertEquals(car, actual);

    }

    @Test
    void should_return_corresponding_car_when_fetch_given_parking_lot_and_two_tickets_and_two_cars(){
        //given
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy();
        standardParkingBoy.manageParkingLot(new ParkingLot());
        Car car1 = new Car();
        ParkingTicket parkingTicket1 = standardParkingBoy.park(car1);

        Car car2 = new Car();
        ParkingTicket parkingTicket2 = standardParkingBoy.park(car2);

        //when
        Car actualCar1 = standardParkingBoy.fetch(parkingTicket1);
        Car actualCar2 = standardParkingBoy.fetch(parkingTicket2);

        //then
        assertEquals(car1, actualCar1);
        assertEquals(car2, actualCar2);
    }

    @Test
    void should_return_null_when_fetch_given_parking_lot_and_wrong_ticket(){
        //given
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy();
        standardParkingBoy.manageParkingLot(new ParkingLot());
        Car car = new Car();
        ParkingTicket parkingTicket = standardParkingBoy.park(car);
        ParkingTicket wrongTicket = new ParkingTicket(888, parkingTicket.getParkingLotId());

        //then
        Throwable actual = assertThrows(UnrecognizedTicketException.class, () -> {
            standardParkingBoy.fetch(wrongTicket);
        });

        assertEquals("Unrecognized parking ticket.", actual.getMessage());
    }

    @Test
    void should_return_null_when_fetch_given_parking_lot_and_used_ticket(){
        //given
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy();
        standardParkingBoy.manageParkingLot(new ParkingLot());
        Car car = new Car();
        ParkingTicket parkingTicket = standardParkingBoy.park(car);

        //when
        standardParkingBoy.fetch(parkingTicket);

        //then
        Throwable actual = assertThrows(UnrecognizedTicketException.class, () -> {
            standardParkingBoy.fetch(parkingTicket);
        });

        assertEquals("Unrecognized parking ticket.", actual.getMessage());
    }

    @Test
    void should_return_null_when_park_given_full_parking_lot() {
        //given
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy();
        standardParkingBoy.manageParkingLot(new ParkingLot(1));
        Car car1 = new Car();
        standardParkingBoy.park(car1);
        Car car2 = new Car();

        //then
        Throwable actual = assertThrows(UnrecognizedTicketException.class, () -> {
            standardParkingBoy.park(car2);
        });

        assertEquals("No available position.", actual.getMessage());
    }

}
