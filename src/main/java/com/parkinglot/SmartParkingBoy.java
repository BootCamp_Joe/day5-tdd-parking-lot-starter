package com.parkinglot;

import java.util.Comparator;

public class SmartParkingBoy extends ParkingBoy{

    public SmartParkingBoy() {}

    @Override
    public ParkingLot getAvailabeParkingLot() {
        return parkingLots.stream()
                .max(Comparator.comparingInt(ParkingLot::getNumberOfEmptyPositions))
                .orElse(null);
    }
}
