package com.parkinglot;

public class ParkingTicket {
    private Integer id;
    private Integer parkingLotId;

    public ParkingTicket(Integer id, Integer parkingLotId) {
        this.id = id;
        this.parkingLotId = parkingLotId;
    }

    public Integer getId() {
        return id;
    }

    public Integer getParkingLotId() {
        return parkingLotId;
    }

}
