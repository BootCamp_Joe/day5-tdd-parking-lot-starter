package com.parkinglot;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

public class ParkingLot {
    private Integer id;
    private List<Car> cars = new ArrayList<>();
    private Integer ticketId = 1;
    private Integer capacity;

    public ParkingLot(Integer capacity) {
        this.capacity = capacity;
    }
    public ParkingLot() {
        this.capacity = 10;
    }

    public boolean isParkingLotFull(){
        return cars.size() == capacity;
    }


    public ParkingTicket park(Car car) {
        if (!cars.contains(car)){
            car.setTicketId(ticketId);
            car.setParkingLotId(id);
            cars.add(car);
            return new ParkingTicket(ticketId++, id);
        }
        return null;
    }

    public Optional<Car> fetch(ParkingTicket parkingTicket) {
        Optional<Car> parkedCar = cars.stream().
                filter(car -> Objects.equals(car.getTicketId(), parkingTicket.getId()) &&
                        Objects.equals(car.getParkingLotId(), parkingTicket.getParkingLotId()))
                .findFirst();
        if (parkedCar.isEmpty()){
            return Optional.empty();
        }else {
            cars.remove(parkedCar.get());
            return parkedCar;
        }
    }

    public Integer getNumberOfEmptyPositions() {
        return capacity - cars.size();
    }

    public Integer getCapacity() {
        return capacity;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
