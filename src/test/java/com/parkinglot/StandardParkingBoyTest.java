package com.parkinglot;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class StandardParkingBoyTest {

    @Test
    void should_park_at_first_parking_lot_when_park_given_two_available_parking_lots() {
        //given
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy();
        standardParkingBoy.manageParkingLot(new ParkingLot());
        standardParkingBoy.manageParkingLot(new ParkingLot());
        Car car = new Car();

        //when
        ParkingTicket parkingTicket = standardParkingBoy.park(car);

        //then
        assertEquals(1, parkingTicket.getParkingLotId());
    }

    @Test
    void should_park_at_second_parking_lot_when_park_given_full_and_available_parking_lots() {
        //given
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy();
        standardParkingBoy.manageParkingLot(new ParkingLot(1));
        standardParkingBoy.manageParkingLot(new ParkingLot());
        Car car1 = new Car();
        Car car2 = new Car();
        standardParkingBoy.park(car1);

        //when
        ParkingTicket parkingTicket = standardParkingBoy.park(car2);

        //then
        assertEquals(2, parkingTicket.getParkingLotId());
    }

    @Test
    void should_return_corresponding_car_when_fetch_given_two_parking_lots_and_two_tickets_and_two_cars() {
        //given
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy();
        standardParkingBoy.manageParkingLot(new ParkingLot());
        standardParkingBoy.manageParkingLot(new ParkingLot());
        Car car1 = new Car();
        ParkingTicket parkingTicket1 = standardParkingBoy.park(car1);

        Car car2 = new Car();
        ParkingTicket parkingTicket2 = standardParkingBoy.park(car2);

        assertEquals(1, parkingTicket1.getParkingLotId());
        assertEquals(1, parkingTicket2.getParkingLotId());

        //when
        Car actualCar1 = standardParkingBoy.fetch(parkingTicket1);
        Car actualCar2 = standardParkingBoy.fetch(parkingTicket2);

        //then
        assertEquals(car1, actualCar1);
        assertEquals(car2, actualCar2);
    }
    @Test
    void should_return_null_when_fetch_given_two_parking_lots_and_wrong_ticket(){
        //given
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy();
        standardParkingBoy.manageParkingLot(new ParkingLot());
        standardParkingBoy.manageParkingLot(new ParkingLot());
        Car car = new Car();
        ParkingTicket parkingTicket = standardParkingBoy.park(car);
        ParkingTicket wrongTicket = new ParkingTicket(888, parkingTicket.getParkingLotId());

        //then
        Throwable actual = assertThrows(UnrecognizedTicketException.class, () -> {
            standardParkingBoy.fetch(wrongTicket);
        });

        assertEquals("Unrecognized parking ticket.", actual.getMessage());
    }
    @Test
    void should_return_null_when_fetch_given_two_parking_lots_and_used_ticket(){
        //given
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy();
        standardParkingBoy.manageParkingLot(new ParkingLot());
        standardParkingBoy.manageParkingLot(new ParkingLot());
        Car car = new Car();
        ParkingTicket parkingTicket = standardParkingBoy.park(car);

        //when
        standardParkingBoy.fetch(parkingTicket);

        //then
        Throwable actual = assertThrows(UnrecognizedTicketException.class, () -> {
            standardParkingBoy.fetch(parkingTicket);
        });

        assertEquals("Unrecognized parking ticket.", actual.getMessage());
    }

    @Test
    void should_return_null_when_park_given_two_full_parking_lots() {
        //given
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy();
        standardParkingBoy.manageParkingLot(new ParkingLot(1));
        standardParkingBoy.manageParkingLot(new ParkingLot(1));
        Car car1 = new Car();
        standardParkingBoy.park(car1);
        Car car2 = new Car();
        standardParkingBoy.park(car2);

        Car car3 = new Car();

        //then
        Throwable actual = assertThrows(UnrecognizedTicketException.class, () -> {
            standardParkingBoy.park(car3);
        });

        assertEquals("No available position.", actual.getMessage());
    }

}
