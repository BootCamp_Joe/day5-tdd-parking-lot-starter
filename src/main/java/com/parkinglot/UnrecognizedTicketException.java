package com.parkinglot;

public class UnrecognizedTicketException extends RuntimeException {
    private String message;

    public UnrecognizedTicketException(String message) {
        System.out.println(message);
        this.message = message;
    }
    @Override
    public String getMessage() {
        return message;
    }
}
